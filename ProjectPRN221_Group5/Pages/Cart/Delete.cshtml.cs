using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221_Group5.Models;
using System.Text.Json;

namespace ProjectPRN221_Group5.Pages.Cart
{
    public class DeleteModel : PageModel
    {
        private readonly PRN221DBContext _db;
        public DeleteModel(PRN221DBContext db)
        {
            _db = db;
        }


        [BindProperty]
        public List<CartItem> Items { get; set; } = new List<CartItem>();
        public async Task<IActionResult> OnGet(int id)
        {
            var carti = HttpContext.Session.GetString("cart");
            if (carti != null)
            {
                Items = JsonSerializer.Deserialize<List<CartItem>>(carti);
            }
            var cusstring = HttpContext.Session.GetString("customer");
            if (cusstring != null)
            {
                var cus = JsonSerializer.Deserialize<Models.Customer>(cusstring) as Customer;
                // var acc = cus.Accounts.FirstOrDefault(a =>a.CustomerId.Equals(cus.CustomerId));



                var product = _db.Products.Where(x => x.ProductId == id).SingleOrDefault() as Models.Product;
                if (product != null)
                {
                    var ci = Items.FirstOrDefault(x => x._shoppingProduct.ProductId == product.ProductId);
                    Items.Remove(ci);
                    if (Items.Count() == 0)
                    {
                        ViewData["msg"] = "Nothing in cart";
                        HttpContext.Session.Remove("cart");
                        return RedirectToPage("CartControl");
                    }

                    var itemses = JsonSerializer.Serialize(Items) as string;
                    HttpContext.Session.SetString("cart", itemses);
                    return RedirectToPage("CartControl");
                }
                return RedirectToPage("CartControl");
            }
            else return RedirectToPage("../Account/Login");
        }
    }
}
