using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Models;
using System.Text.Json;

namespace ProjectPRN221_Group5.Pages.Cart
{
    public class CartControlModel : PageModel
    {
        private readonly PRN221DBContext _db;
        public CartControlModel(PRN221DBContext db)
        {
            _db = db;
        }
        [BindProperty]
        public DateTime RequireDate { get; set; }
        [BindProperty]
        public List<CartItem> Items { get; set; } = new List<CartItem>();

        [BindProperty]
        public Customer Customer { get; set; }
        public async Task<IActionResult> OnGet(int id)
        {
            RequireDate = DateTime.Now;
            var carti = HttpContext.Session.GetString("cart");
            if (carti != null)
            {
                Items = JsonSerializer.Deserialize<List<CartItem>>(carti);
             
            }


            var cusstring = HttpContext.Session.GetString("customer");
            if (cusstring != null)
            {
                var cus = JsonSerializer.Deserialize<Models.Customer>(cusstring) as Customer;
                // var acc = cus.Accounts.FirstOrDefault(a =>a.CustomerId.Equals(cus.CustomerId));
                Customer customer = _db.Customers.Where(c => c.CustomerId.Equals(cus.CustomerId)).FirstOrDefault();
                Customer = customer;


                var product = _db.Products.Where(x => x.ProductId == id).SingleOrDefault() as Models.Product;
                if (product != null)
                {

                    if (Items.Find(x => x._shoppingProduct.ProductId == product.ProductId) != null)
                    {
                        CartItem cie = Items.FirstOrDefault(i => i._shoppingProduct.ProductId == product.ProductId);
                        cie.quantity += 1;
                    }
                    else
                    {
                        CartItem ci = new CartItem();
                        ci.Id = cus.CustomerId;
                        ci._shoppingProduct = product;
                        ci.quantity = 1;
                        Items.Add(ci);

                    }
                    var itemses = JsonSerializer.Serialize(Items) as string;
                    HttpContext.Session.SetString("cart", itemses);
                    
                    return Page();
                }
                return Page();
            }
            else return RedirectToPage("../Account/Login");


        }

        public async Task<IActionResult> OnPostAsync()
        {   
            var carti = HttpContext.Session.GetString("cart");
            if(carti == null) {
                ViewData["msg"] = "Nothing in your Cart, Go to shopping now <3";
                return Page();
            }
           else {
                List<CartItem> carti_items = new List<CartItem>();
                carti_items = JsonSerializer.Deserialize<List<CartItem>>(carti);



                var cusstring = HttpContext.Session.GetString("customer");
                var cus = JsonSerializer.Deserialize<Models.Customer>(cusstring) as Customer;
                // var acc = cus.Accounts.FirstOrDefault(a =>a.CustomerId.Equals(cus.CustomerId));
                Customer customer = _db.Customers.Where(c => c.CustomerId.Equals(cus.CustomerId)).FirstOrDefault();
                Customer = customer;
                Models.Order order = new Models.Order();
                order.CustomerId = customer.CustomerId;
                order.OrderDate = DateTime.Now;
                if (RequireDate <= order.OrderDate)
                {
                    ViewData["msg"] = "Required Date is invalid";
                    return Redirect("CartControl");
                }
                order.ShipAddress = Customer.Address;
                order.ShippedDate = DateTime.Now.AddDays(10);
                order.RequiredDate = RequireDate;

                await _db.Orders.AddAsync(order);
                await _db.SaveChangesAsync();


                foreach (var item in carti_items)
                {
                    var product = _db.Products.Where(x => x.ProductId == item._shoppingProduct.ProductId).SingleOrDefault() as Models.Product;
                    OrderDetail od = new OrderDetail();
                    od.OrderId = order.OrderId;
                    od.ProductId = product.ProductId;
                    od.Quantity = (short)item.quantity;
                    od.UnitPrice = (decimal)product.UnitPrice;
                    od.Discount = 0;

                    await _db.OrderDetails.AddAsync(od);

                }
                await _db.SaveChangesAsync();
                HttpContext.Session.Remove("cart");
                ViewData["msg"] = "Order Successful!!!!";




                return Page();
            }

            
        }
    }
}
