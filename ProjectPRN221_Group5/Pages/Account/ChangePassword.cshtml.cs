using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Account
{
    public class ChangePasswordModel : PageModel
    {
        private readonly PRN221DBContext _context;

        public ChangePasswordModel(PRN221DBContext context)
        {
            _context = context;
        }

        public IActionResult OnPost(string email, string oldpass, string newpass, string newpassagain)
        {
            var _user = _context.Accounts.FirstOrDefault(x => x.Email == email && x.Password == oldpass);
            var alert = "";
            if (_user != null)
            {
                if (newpass == newpassagain)
                {
                    _user.Password = newpass;
                    _context.SaveChanges();
                    alert = "Change password successful";
                }
                else
                {
                    alert = "* New PassWord again not true";
                }
            }
            else
            {
                alert = "* Something was wrong";
            }
            ViewData["msg"] = alert;
            return Page();
        }
    }
}
