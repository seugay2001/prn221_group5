using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Helpers;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Account
{
    public class ForgotPasswordModel : PageModel
    {
        private readonly PRN221DBContext _db;
        public ForgotPasswordModel(PRN221DBContext db)
        {
            _db = db;
        }
        [BindProperty]
        public string email { get; set; }
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("customer") != null || HttpContext.Session.GetString("admin") != null)
            {
                return RedirectToPage("./404Page");
            }

            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (String.IsNullOrEmpty(email))
            {
                TempData["msg"] = "Please enter your email to get password!";
                return Page();
            }

            var account = await _db.Accounts.FirstOrDefaultAsync(a => a.Email.Equals(email));
            if (account == null)
            {
                TempData["msg"] = "Not found email, please check again!";
                return Page();
            }
            String password = account.Password;

            string bodyMail = "Your password is: " + password + "";

            var body = bodyMail;
            SendPasswordMail.SendMail(email, body);
            TempData["msg"] = "Please check you email!";
            return Page();

        }
    }
}
