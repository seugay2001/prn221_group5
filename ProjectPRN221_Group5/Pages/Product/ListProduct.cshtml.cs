using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221_Group5.Models;
using System.Text.Json;

namespace ProjectPRN221_Group5.Pages.Product
{
    public class ListProductModel : PageModel
    {
        private readonly PRN221DBContext dbContext;

        public ListProductModel(PRN221DBContext dbContext)
        {
            this.dbContext = dbContext;
        }

       
        [BindProperty]
        public List<Category> Categories { get; set; }

        [BindProperty]
        public List<Models.Product> Products { get; set; } = new List<Models.Product>();

        [BindProperty]
        public List<Models.Product> NewProducts { get; set; }

        [FromQuery(Name = "id")]
        public string CatId { get; set; }

        public void OnGet()
        {
            Categories = dbContext.Categories.ToList();
            var products = dbContext.Products.ToList();

            if (CatId != null)
            {
                Products = dbContext.Products
                    .Where(p => p.CategoryId == Int32.Parse(CatId))
                    .ToList();
            }
            else
            {
                NewProducts = dbContext.Products
                    .OrderByDescending(p => p.ProductId).Take(8).ToList();
            }

            var cart = HttpContext.Session.GetString("cart");

            
        }
    }
}
