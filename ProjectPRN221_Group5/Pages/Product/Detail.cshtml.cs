using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Models;
using System.Text.Json;

namespace ProjectPRN221_Group5.Pages.Product
{
    public class DetailModel : PageModel
    {
        private readonly PRN221DBContext dbContext;

        public DetailModel(PRN221DBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        

        [BindProperty]
        public Models.Product Product { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Product = await dbContext.Products.FirstOrDefaultAsync(m => m.ProductId == id);
            var cat = await dbContext.Categories.ToListAsync();

            var cart = HttpContext.Session.GetString("cart");

           

            if (Product == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Product = await dbContext.Products.FirstOrDefaultAsync(m => m.ProductId == id);
            var cat = await dbContext.Categories.ToListAsync();

            if (Product == null)
            {
                return NotFound();
            }
            return Page();
        }


    }
}
