using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Product
{
    public class UpdateModel : PageModel
    {
        private readonly PRN221DBContext _db;
        private readonly IHubContext<SignalHub> _hubContext;
        public UpdateModel(PRN221DBContext db, IHubContext<SignalHub> hubContext)
        {
            _db = db;
            _hubContext = hubContext;
        }

        [BindProperty]
        public Models.Product Product { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                ViewData["Category"] = new SelectList(_db.Categories, "CategoryId", "CategoryName");
                var product = await _db.Products.FindAsync(id);
                if (product != null)
                    Product = product;
                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Category"] = new SelectList(_db.Categories, "CategoryId", "CategoryName");
                return Page();
            }
            var product = await _db.Products.FindAsync(id);
            product.ProductName = Product.ProductName;
            product.UnitPrice = Product.UnitPrice;
            product.QuantityPerUnit = Product.QuantityPerUnit;
            product.CategoryId = Product.CategoryId;
            product.Discontinued = Product.Discontinued;
            product.UnitsInStock= Product.UnitsInStock;
            await _db.SaveChangesAsync();
            //TempData["msg"] = "Update success.";
            await _hubContext.Clients.All.SendAsync("ReloadData");
            return RedirectToPage("./List");
        }
    }
}

