using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Product
{
    public class DetailModel : PageModel
    {
        private readonly PRN221DBContext _db;
        public DetailModel(PRN221DBContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Models.Product Product { get; set; }
        [BindProperty]
        public string Category { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                var product = await _db.Products.FindAsync(id);
                if (product != null)
                    Product = product;
                Category = _db.Categories.First(c => c.CategoryId == product.CategoryId).CategoryName;
                return Page();
            }
        }
    }
}
