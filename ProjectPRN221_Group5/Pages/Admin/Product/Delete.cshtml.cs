using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;
using System;

namespace ProjectPRN221_Group5.Pages.Admin.Product
{
    public class DeleteModel : PageModel
    {
        private readonly PRN221DBContext _context;
        private readonly IHubContext<SignalHub> _hubContext;
        public DeleteModel(PRN221DBContext context, IHubContext<SignalHub> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }

        [BindProperty]
        public Models.Product Product { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return RedirectToPage("/Account/404Page");
            }
            var count = _context.OrderDetails.Where(od => od.ProductId == id).Count();
            if (count > 0)
            {
                TempData["msg"] = "This product existed in Order details.";
                return RedirectToPage("./List");
            }
            var product = await _context.Products.FindAsync(id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("ReloadData");
            TempData["msg"] = "Product has been deleted successfully."; // Th�ng b�o x�a th�nh c�ng

            return RedirectToPage("./List");

        }
    }
}
