using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;
using System.Security.Principal;
using System.Text.Json;

namespace ProjectPRN221_Group5.Pages.Admin.Product
{
    public class ListModel : PageModel
    {
        private readonly PRN221DBContext _db;
        private readonly IHubContext<SignalHub> _hubContext;
        public ListModel(PRN221DBContext db, IHubContext<SignalHub> hubContext)
        {
            _db = db;
            _hubContext = hubContext;
        }
        public const int pageSize = 10;
        [BindProperty(SupportsGet = true, Name = "currentPage")]
        public int currentPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public string search { get; set; }
        [BindProperty(SupportsGet = true)]
        public int categoryChoose { get; set; }
        public int countPages { get; set; }
        [BindProperty]
        public List<Models.Category> Categories { get; set; }
        [BindProperty]
        public List<Models.Product> Products { get; set; }
        public async Task<IActionResult> OnGetAsync()
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                Categories = _db.Categories.ToList();
                int totalProduct = getTotalProducts();
                countPages = (int)Math.Ceiling((double)totalProduct / pageSize);
                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                if (currentPage > countPages)
                {
                    currentPage = countPages;
                }
                if (!string.IsNullOrEmpty(search))
                {
                    var qr = (from a in _db.Products orderby a.ProductId ascending select a).Where(a => a.ProductName.Contains(search)).Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Products = await qr.ToListAsync();
                }
                else if (categoryChoose > 0)
                {
                    var qr = (from p in _db.Products select p).Where(p => p.CategoryId == categoryChoose)
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Products = await qr.ToListAsync();
                }
                else if (categoryChoose > 0 && !string.IsNullOrEmpty(search))
                {
                    var qr = (from p in _db.Products select p).Where(p => p.CategoryId == categoryChoose && p.ProductName.Contains(search))
                        .Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Products = await qr.ToListAsync();
                }
                else
                {
                    var qr = (from a in _db.Products orderby a.ProductId ascending select a).Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Products = await qr.ToListAsync();
                }
                return Page();
            }
        }

        private int getTotalProducts()
        {
            var list = (from p in _db.Products select p).ToList();

            if (!String.IsNullOrEmpty(search))
            {
                return list.Where(p => p.ProductName.ToLower().Contains(search.ToLower())).ToList().Count;
            }
            else if (categoryChoose > 0)
            {
                return list.Where(p => p.CategoryId == categoryChoose).ToList().Count;
            }
            else
            {
                return list.Count;
            }
        }
    }
}
