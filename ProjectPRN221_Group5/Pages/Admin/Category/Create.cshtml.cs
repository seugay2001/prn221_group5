using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Category
{
    public class CreateModel : PageModel
    {
        private readonly PRN221DBContext _db;
        private readonly IHubContext<SignalHub> _hubContext;
        public CreateModel(PRN221DBContext db, IHubContext<SignalHub> hubContext)
        {
            _db = db;
            _hubContext = hubContext;
        }

        [BindProperty]
        public Models.Category Category { get; set; }
        public async Task<IActionResult> OnGetAsync()
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();
            await _db.Categories.AddAsync(Category);
            await _db.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("ReloadData");
            return RedirectToPage("List");
        }
    }
}
