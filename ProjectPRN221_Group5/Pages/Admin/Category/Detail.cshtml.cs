using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Category
{
    public class DetailModel : PageModel
    {
        private readonly PRN221DBContext _db;
        public DetailModel(PRN221DBContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Models.Category Category { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                Category = await _db.Categories.FindAsync(id);
                return Page();
            }
        }
    }
}
