using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Category
{
    public class ListModel : PageModel
    {
        private readonly PRN221DBContext _db;
        private readonly IHubContext<SignalHub> _hubContext;
        public ListModel(PRN221DBContext db, IHubContext<SignalHub> hubContext)
        {
            _db = db;
            _hubContext = hubContext;
        }
        public const int pageSize = 5;
        [BindProperty(SupportsGet = true, Name = "currentPage")]
        public int currentPage { get; set; }
        [BindProperty(SupportsGet = true)]
        public string search { get; set; }
        [BindProperty(SupportsGet = true)]
        public int categoryChoose { get; set; }
        public int countPages { get; set; }
        [BindProperty]
        public List<Models.Category> Categories { get; set; }
        public async Task<IActionResult> OnGetAsync()
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                int totalCategory = getTotalCategories();
                countPages = (int)Math.Ceiling((double)totalCategory / pageSize);

                if (currentPage < 1)
                {
                    currentPage = 1;
                }
                if (currentPage > countPages)
                {
                    currentPage = countPages;
                }
                if (!string.IsNullOrEmpty(search))
                {
                    var qr = (from a in _db.Categories orderby a.CategoryId ascending select a).Where(a => a.CategoryName.Contains(search)).Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Categories = await qr.ToListAsync();
                }
                else
                {
                    var qr = (from a in _db.Categories orderby a.CategoryId ascending select a).Skip((currentPage - 1) * pageSize).Take(pageSize);
                    Categories = await qr.ToListAsync();
                }
                return Page();
            }
        }

        public async Task<IActionResult> OnGetDeleteAsync(int? id)
        {
            if (id == null)
            {
                return RedirectToPage("/Account/404Page");
            }
            var count = _db.Products.Where(od => od.CategoryId == id).Count();
            if (count > 0)
            {
                TempData["msg"] = "This category existed in Products.";
                return RedirectToPage("./List");
            }
            var category = await _db.Categories.FindAsync(id);
            _db.Categories.Remove(category);
            await _db.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("ReloadData");
            return RedirectToPage("./List");
        }

        private int getTotalCategories()
        {
            var list = (from p in _db.Categories select p).ToList();

            if (!String.IsNullOrEmpty(search))
            {
                return list.Where(p => p.CategoryName.ToLower().Contains(search.ToLower())).ToList().Count;
            }
            else
            {
                return list.Count;
            }
        }
    }
}
