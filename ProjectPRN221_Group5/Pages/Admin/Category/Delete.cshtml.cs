using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

namespace ProjectPRN221_Group5.Pages.Admin.Category
{
    public class DeleteModel : PageModel
    {
        private readonly PRN221DBContext _context;
        private readonly IHubContext<SignalHub> _hubContext;
        public DeleteModel(PRN221DBContext context, IHubContext<SignalHub> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }

        [BindProperty]
        public Models.Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return RedirectToPage("/Account/404Page");
            }
            var count = _context.Products.Where(od => od.CategoryId == id).Count();
            if (count > 0)
            {
                TempData["msg"] = "This category existed in Product.";
                return RedirectToPage("./List");
            }
            var category = await _context.Categories.FindAsync(id);
            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("ReloadData");
            return RedirectToPage("./List");
        }
    }
}
