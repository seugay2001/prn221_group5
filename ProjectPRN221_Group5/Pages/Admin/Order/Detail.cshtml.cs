using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221_Group5.Models;
using Microsoft.EntityFrameworkCore;

namespace ProjectPRN221_Group5.Pages.Admin.Order
{
    public class DetailModel : PageModel
    {
        private readonly PRN221DBContext _db;

        public DetailModel(PRN221DBContext db)
        {
            _db = db;
        }

        [BindProperty]
		public Models.Order order { get; set; }
		public async Task<IActionResult> OnGetAsync(int? id)
		{
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                if (id == null)
                {
                    return RedirectToPage("/Account/404Page");
                }
                order = await _db.Orders.Include(o => o.Employee).Include(o => o.Customer).FirstOrDefaultAsync(o => o.OrderId == id);
                order.OrderDetails = (from od in _db.OrderDetails select od)
                    .Where(o => o.OrderId == order.OrderId)
                    .ToList();
                foreach (var o in order.OrderDetails)
                {
                    o.Product = await _db.Products.FirstOrDefaultAsync(p => p.ProductId == o.ProductId);
                }
                return Page();
            }
		}
	}
}
