using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectPRN221_Group5.Models;
using Microsoft.EntityFrameworkCore;

namespace ProjectPRN221_Group5.Pages.Admin.Dashboard
{
    public class IndexModel : PageModel
    {
        public PRN221DBContext db;
        public double wsale { get; set; }
        public long totalOrder { get; set; }
        public long totalCus { get; set; }
        public long totalGuest { get; set; }

        public IndexModel(PRN221DBContext dBContext)
        {
            db = dBContext;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            if (HttpContext.Session.GetString("admin") == null)
                return RedirectToPage("/Account/404Page");
            else
            {
                decimal wsales = 0;
                DateTime from = DateTime.Now.AddDays(-7);
                List<Models.Order> lo = db.Orders.Include(o => o.OrderDetails)
                    .Where(o => o.ShippedDate >= from && o.ShippedDate <= DateTime.Now).ToList();
                lo.ForEach(o =>
                {
                    List<Models.OrderDetail> lod = o.OrderDetails.ToList();
                    wsales += lod.Sum(o => o.Quantity * o.UnitPrice * (1 - (decimal)o.Discount));
                });
                ViewData["totalO"] = db.Orders.Count();
                ViewData["totalC"] = db.Customers.Count();
                ViewData["totalG"] = db.Accounts.Count();
                ViewData["wsales"] = wsales;
                return Page();
            }
        }

    }
}
