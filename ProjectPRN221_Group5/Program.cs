using Microsoft.EntityFrameworkCore;
using ProjectPRN221_Group5.Hubs;
using ProjectPRN221_Group5.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddSession(otp => otp.IdleTimeout = TimeSpan.FromMinutes(5));
builder.Services.AddDbContext<PRN221DBContext>(opt =>
{
    opt.UseSqlServer(builder.Configuration.GetConnectionString("PRN221DB"));
});
builder.Services.AddSignalR();
var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseSession();
app.UseAuthorization();
app.MapHub<SignalHub>("/hub");
app.MapRazorPages();

app.Run();
